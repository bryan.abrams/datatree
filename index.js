
const NODE = (value, color, children={}) => ({ data: { value, color }, ...children })

let A = NODE('Appel', '#ff0000')
let B = NODE('Jus d\'orange', '#ffb400')
let C = NODE('Kots', '#c6ff00')
let D = NODE('Witte druif', '#00ff7e')
let E = NODE('Minder blauwe bes', '#00aeff')
let F = NODE('Blauwe bes', '#2a00ff')
let G = NODE('Kiwi', '#18ff00')
let H = NODE('Rode druif', '#f000ff')
let I = NODE('Pompoen', '#ff5a00')
let J = NODE('Bal', '#ff007e')

I = {...I, J}
E = {...E, F}
C = {...C, D, E}
G = {...G, H}
A = {...A, B, C, G}

module.exports = NODE('Root', '#000000', {A, I})

